package com.cammy.logging.layout

import argonaut.{EncodeJson, Argonaut}
import com.cammy.logging.domain.message._
import org.scalatest.{Matchers, FunSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import Argonaut._
import LogMessageEncoder._

class LogMessageEncoderTest  extends FunSpec
  with GeneratorDrivenPropertyChecks
  with Matchers {

  describe("Log encoder") {
    describe("#meta encoder") {


      case class MetaKey(key : String)
      case class MetaValue(key : String)

      case class Meta1(metaInfo : Map[String, String])

      implicit def encodeMeta1: EncodeJson[Meta1] =
        EncodeJson((ts: Meta1) => ("meta" := ts.metaInfo.asJson) ->: jEmptyObject)

      val meta2 = Meta1(Map("tag" -> "value",
                            "tag1" -> "value1"))

      //println(meta2.asJson)

    }
  }
}
