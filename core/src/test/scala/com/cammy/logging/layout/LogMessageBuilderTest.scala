package com.cammy.logging.layout

import java.util.UUID

import com.cammy.logging.domain.message._
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FunSpec, Matchers}

class LogMessageBuilderTest
    extends FunSpec
    with GeneratorDrivenPropertyChecks
    with Matchers {

  describe("Log message builder") {

    describe("optional field") {

      it("Should not include 'None'") {

        val sessionBuilder = LogMessageBuilder.build("sampleApp","0.0.1","Dev",Some("test-host" ))
        val dynamicBuilder = sessionBuilder(Some(UUID.fromString("f4bfa72b-367e-461c-9904-c7e447f2b14a")), Map(), Map())
        val formattedMsg = dynamicBuilder("test-msg",Some(LogLevel.INFO),"2016-11-28T04:58:42.187Z",  None, Map("meta1"-> "value1", "meta1"-> "value1") )

        val expectedFormattedMsg = "{\"@timestamp\":\"2016-11-28T04:58:42.187Z\",\"correlation_id\":\"f4bfa72b-367e-461c-9904-c7e447f2b14a\",\"app_version\":\"0.0.1\",\"tags\":{},\"log_level\":\"info\",\"meta\":{\"meta1\":\"value1\"},\"app_name\":\"sampleApp\",\"message\":\"test-msg\",\"instance_id\":\"test-host\",\"env\":\"Dev\"}"

        formattedMsg should equal (expectedFormattedMsg)
      }

      it("Should exclude empty tags") {

        val sessionBuilder = LogMessageBuilder.build("sampleApp","0.0.1","Dev",Some("test-host" ))
        val dynamicBuilder = sessionBuilder(Some(UUID.fromString("f4bfa72b-367e-461c-9904-c7e447f2b14a")), Map("emptyKey" -> ""), Map())
        val formattedMsg = dynamicBuilder("test-msg",Some(LogLevel.INFO),"2016-11-28T04:58:42.187Z",  None, Map("meta1"-> "value1", "meta1"-> "value1") )

        val expectedFormattedMsg = "{\"@timestamp\":\"2016-11-28T04:58:42.187Z\",\"correlation_id\":\"f4bfa72b-367e-461c-9904-c7e447f2b14a\",\"app_version\":\"0.0.1\",\"tags\":{},\"log_level\":\"info\",\"meta\":{\"meta1\":\"value1\"},\"app_name\":\"sampleApp\",\"message\":\"test-msg\",\"instance_id\":\"test-host\",\"env\":\"Dev\"}"

        formattedMsg should equal (expectedFormattedMsg)
      }
    }

  }
}
