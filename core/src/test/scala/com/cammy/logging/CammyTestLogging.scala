package com.cammy.logging

import com.cammy.logging.logger.impl.{TaggedCammyLogger, DefaultCammyLogger}
import com.cammy.logging.domain.message.{MetaKey, TagKey}
import com.typesafe.scalalogging.{Logger => TypeSafeLogger}
import org.slf4j.LoggerFactory


trait CammyTestLogging {

  lazy val typeSafeLogger= TypeSafeLogger(LoggerFactory.getLogger("test"))

  val testTag = Map("tagKey" -> "tagValue")

  lazy val logger = DefaultCammyLogger(typeSafeLogger)
  lazy val taggedLogger = TaggedCammyLogger(testTag, typeSafeLogger)


}

