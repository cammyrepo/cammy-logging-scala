package com.cammy.logging.logback

import java.util.UUID

import argonaut.Argonaut._
import com.cammy.logging.CammyTestLogging
import com.cammy.logging.domain.message.ESFieldName._
import com.cammy.logging.domain.message._
import com.cammy.logging.layout.LogMessageEncoder._
import com.cammy.logging.layout.LogMessageBuilder
import com.typesafe.scalalogging.{Logger => TypeSafeLogger}
import org.scalatest.mock.MockitoSugar
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FunSpec, GivenWhenThen, Matchers}


class CammyLogLayoutSpec extends FunSpec with GeneratorDrivenPropertyChecks with GivenWhenThen with MockitoSugar with Matchers with CammyTestLogging {

  val cammyLayout = new JSONLayout()


  describe("cammy layout") {

    describe("#log message") {

      it("should not include optional field, if empty") {

        val sessionBuilder = LogMessageBuilder.build("sampleApp", "0.0.1", "Dev", Some("test-host"))
        val dynamicBuilder = sessionBuilder(Some(UUID.fromString("f4bfa72b-367e-461c-9904-c7e447f2b14a")), Map(), Map())
        val formattedMsg = dynamicBuilder("test-msg",
          Some(LogLevel.INFO),
          "2016-11-28T04:58:42.187Z",
          None,
          Map("meta1" -> "value1", "meta1" -> "value1"))

        val expectedFormattedMsg = "{\"@timestamp\":\"2016-11-28T04:58:42.187Z\",\"correlation_id\":\"f4bfa72b-367e-461c-9904-c7e447f2b14a\",\"app_version\":\"0.0.1\",\"tags\":{},\"log_level\":\"info\",\"meta\":{\"meta1\":\"value1\"},\"app_name\":\"sampleApp\",\"message\":\"test-msg\",\"instance_id\":\"test-host\",\"env\":\"Dev\"}"


        formattedMsg should equal(expectedFormattedMsg)

        val logMessage = LogMessage("sampleApp", "0.0.1", "Dev", Some("test-host"), "2016-11-28T04:58:42.187Z", Some(LogLevel.INFO), Some(UUID.fromString("f4bfa72b-367e-461c-9904-c7e447f2b14a")), "test-msg", None, Tags(Map(
          "tag1" -> "value1")), Meta(Map("meta1" -> "value1")))

        val jsonLog = "{\"@timestamp\":\"2016-11-28T04:58:42.187Z\",\"correlation_id\":\"f4bfa72b-367e-461c-9904-c7e447f2b14a\",\"app_version\":\"0.0.1\",\"tags\":{\"tag1\":\"value1\"},\"log_level\":\"info\",\"meta\":{\"meta1\":\"value1\"},\"app_name\":\"sampleApp\",\"message\":\"test-msg\",\"instance_id\":\"test-host\",\"env\":\"Dev\"}"

        logMessage.asJson.toString() should equal(jsonLog)
      }

      it("should generate appinfo from conf") {

        val expectedBuildInfo = BuildInfo("testApp", "0.0.1", Option(System.getenv("SCALA_ENV")).getOrElse(""))

        val msgBuildInfo = LogMessageBuilder.buildInfo

        expectedBuildInfo should equal(msgBuildInfo)
      }
    }

  }
}
