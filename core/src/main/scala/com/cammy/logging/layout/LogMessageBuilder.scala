package com.cammy.logging.layout

import java.net._
import java.util.UUID
import argonaut.Argonaut._
import argonaut.PrettyParams
import com.cammy.logging.layout.LogMessageEncoder._
import com.cammy.logging.domain.message.ESFieldName._
import com.cammy.logging.domain.message.{BuildInfo, _}
import com.typesafe.config.ConfigFactory

import scala.util.Try

object LogMessageBuilder {

  val buildInfo = Configuration.buildInfo
  val instanceId = Option(InetAddress.getLocalHost.getHostName)

  lazy val sessionBuilder = build(buildInfo.appName, buildInfo.appVersion, buildInfo.appEnv, instanceId)

  def build(appName: String,
            appVersion: String,
            env: String,
            instanceId: Option[String]) = {

    (correlationId: Option[UUID], sessionTags: Map[String, String], sessionMeta: Map[String, String]) =>
      {

        val tags = Tags(sessionTags.filterNot(_._2.trim.isEmpty))  // filter empty values

        (message: String, logLevel: Option[LogLevel.level], timeStamp: String, exception: Option[LogException], dynamicMeta: Map[String, String]) =>
          {

            val meta = Meta(sessionMeta ++ dynamicMeta)

             LogMessage(appName,
                       appVersion,
                       env,
                       instanceId,
                       timeStamp,
                       logLevel,
                       correlationId,
                       message,
                       exception,
                       tags,
                       meta).asJson.pretty(PrettyParams.nospace.copy(dropNullKeys = true))
          }
      }
  }
}
