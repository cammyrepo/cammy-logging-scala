package com.cammy.logging.layout

import java.util.UUID

import argonaut.Argonaut._
import argonaut.EncodeJson
import com.cammy.logging.domain.message._
import com.cammy.logging.domain.message.ESFieldName._
import org.joda.time.DateTime


object LogMessageEncoder {

  implicit def encodeDateJson: EncodeJson[DateTime] =
    EncodeJson((a: DateTime) => a.toString.asJson)

  implicit def encodeUUIDJson: EncodeJson[UUID] =
    EncodeJson((a: UUID) => a.toString.asJson)

  implicit def encodeTags: EncodeJson[Tags] =
    EncodeJson((ts: Tags) => ts.tags.asJson)

  implicit final val encodeLogLevel: EncodeJson[LogLevel.level] =
    EncodeJson((a: LogLevel.level) => a.toString.toLowerCase.asJson)

  implicit def encodeMeta: EncodeJson[Meta] =
    EncodeJson((ts: Meta) =>  ts.meta.asJson)

  implicit def encodeException: EncodeJson[LogException] =
    EncodeJson((ex: LogException) =>
      (EXCEPTION_CLASS := ex._class) ->: ( EXCEPTION_MESSAGE := ex.message) ->: (EXCEPTION_STACKTRACE := ex.stackTrace) ->: jEmptyObject)

  implicit def encodeMessage[A: EncodeJson]: EncodeJson[LogMessage[A]] =
    EncodeJson(
      (msg: LogMessage[A]) =>
          (APP_NAME := msg.appName) ->:
          (APP_VERSION := msg.appVersion) ->:
          (APP_ENV :=  msg.appEnv) ->:
          msg.instanceId.map(INSTANCE_ID := _) ->?:
          ( TIME_STAMP := msg.timeStamp) ->:
            msg.correlationId.map(CORRELATION_KEY := _) ->?:
          (MESSAGE := msg.message) ->:
          (LOG_LEVEL := msg.level) ->:
          msg.exception.map(EXCEPTION := _) ->?:
          (TAGS := msg.tags) ->:
          (META := msg.meta) ->: jEmptyObject)

}
