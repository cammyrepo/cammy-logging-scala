package com.cammy.logging.layout

import com.cammy.logging.domain.message.BuildInfo
import com.cammy.logging.domain.message.ESFieldName._
import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try


object Configuration {

  lazy val  buildInfo = fromConfigFile().getOrElse(fromManifest.getOrElse(BuildInfo("","", "")))

  private def fromConfigFile() = {

    val appConfig = Try (ConfigFactory.load ().getConfig ("global").getConfig ("appInfo")).toOption
    val buildConfig = Try(ConfigFactory.load("BuildInfo").getConfig ("buildInfo")).toOption

    for {
      config <- appConfig match {
                       case None => buildConfig
                       case Some(a) => Some(a)  }
      appVer <- Try(config.getString(APP_VERSION)).toOption
      appName <- Try(config.getString(APP_NAME)).toOption
      appEnv = Option(System.getenv("SCALA_ENV")).getOrElse("")
    } yield BuildInfo(appName, appVer, appEnv)
  }

  private def fromManifest =
  //val jf = new JarFile(new File(getClass.getProtectionDomain.getCodeSource.getLocation.getPath))
    for {
      manifest <- Try(getClass.getPackage).toOption
      appName <- Option(manifest.getImplementationTitle)
      appVer <- Option(manifest.getImplementationVersion)
      appEnv = Option(System.getenv("SCALA_ENV")).getOrElse("")
    } yield BuildInfo(appName, appVer,appEnv)



  private  def fromLogParam(appInfoParam : String) =
  {
    val FIELD_SEPERATOR : String = ","
    val KEYWORD_SEPERATOR : String = ":"

    val appInfoTags = appInfoParam.split(FIELD_SEPERATOR).toList.flatMap(field => {
      val words = field.split(KEYWORD_SEPERATOR)
      for {
        staticTagKey <- words.headOption
        staticTagValue <- Option(words(1))
      } yield (staticTagKey, staticTagValue)
    })

    for {
      appName <- appInfoTags.find(_._1 == APP_NAME).map(_._2)
      appVer <- appInfoTags.find(_._1 == APP_VERSION).map(_._2)
      appEnv <- appInfoTags.find(_._1 == APP_ENV).map(_._2)
    } yield BuildInfo(appName, appVer, appEnv)
  }



}
