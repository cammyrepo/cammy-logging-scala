package com.cammy.logging

import com.cammy.logging.logger.impl.DefaultCammyLogger
import com.cammy.logging.domain.message.{MetaKey, TagKey}
import com.typesafe.scalalogging.{Logger => TypeSafeLogger}
import org.slf4j.{MDC, LoggerFactory}


trait CammyLogging{

  protected lazy val logger = DefaultCammyLogger(TypeSafeLogger(LoggerFactory.getLogger(getClass.getName)))

}
