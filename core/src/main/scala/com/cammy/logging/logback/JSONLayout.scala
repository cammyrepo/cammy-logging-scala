package com.cammy.logging.logback

import java.util.{UUID, TimeZone}
import com.cammy.logging.logger.impl.TagMarker
import com.cammy.logging.domain.message.ESFieldName._
import ch.qos.logback.classic.Level
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.LayoutBase
import com.cammy.logging.logback.JSONLayout._
import com.cammy.logging.domain.message.{ContextTagKey, LogException, LogLevel}
import com.cammy.logging.layout.LogMessageBuilder
import org.apache.commons.lang.time.FastDateFormat
import scala.beans.BeanProperty
import scala.util.{Properties, Try}
import scalaz.syntax.std.boolean._
import scala.collection.JavaConversions._
import com.cammy.logging.domain.message.MetaKey._

class JSONLayout() extends LayoutBase[ILoggingEvent] {


  override def doLayout(event: ILoggingEvent): String = {

    val timeStamp = dateFormat(event.getTimeStamp)
    val logLevel = toLogLevel(event.getLevel)
    val message = event.getFormattedMessage

    // filter correlation Id , if present
    val (mdcCorrelationId, tags ) =  getTags(event).partition(_._1 == CORRELATION_KEY)
    val correlationId = mdcCorrelationId.get(CORRELATION_KEY).flatMap( id => Try { UUID.fromString(id) }.toOption )
    val meta = getMeta(event)
    val exception = getExceptionInfo(event)

    val dynamicFieldBuilder = LogMessageBuilder.sessionBuilder(correlationId, tags, meta)
    dynamicFieldBuilder(message, Option(logLevel), timeStamp, exception, Map()) + Properties.lineSeparator
  }

   // seesion tags from MDC map
   def getTags(event: ILoggingEvent) = {

     val mdc = event.getMDCPropertyMap
     val contextTags =
       mdc.collect{ case (ContextTagKey(key) , value ) => (key, value)}

    // from markers
     val markerTags = event.getMarker match {
       case tagMarker : TagMarker => tagMarker.tags
       case other => Map()
     }

     markerTags ++ contextTags
  }

  def getMeta(event: ILoggingEvent) = {

    val threadMeta = (THREAD_NAME , event.getThreadName)

    val locationInfoOpt = for {
      shouldInclude <- includeLocationInfo.option("locationInfo")
      callerData <- event.getCallerData.headOption
    } yield callerData

    val meta = locationInfoOpt match {
      case Some(locationInfo)  => List((FILE_NAME, locationInfo.getFileName) ,
        (LINE_NUMBER, locationInfo.getLineNumber.toString),
        (CLASS_NAME, locationInfo.getClassName),
        (METHOD_NAME, locationInfo.getMethodName)) ++ List(threadMeta)
      case None => List(threadMeta)
    }

    meta.toMap
  }

  def getExceptionInfo(logEvent: ILoggingEvent) = {

    for {
      ignore  <- includeThrowableInfo.option("ignore")
      throwableInfo <- Option(logEvent.getThrowableProxy)
      exMsg <- Option(throwableInfo.getMessage)
      className = Option(throwableInfo.getClassName)
      stackTrace = Option(throwableInfo.getStackTraceElementProxyArray.map(_.getSTEAsString).fold("")((init, trace) => init + "\n" + trace))
    } yield LogException(className, Option(exMsg), stackTrace)
  }

  @BeanProperty
  var includeLocationInfo: Boolean = false
  @BeanProperty
  var includeThrowableInfo = true

}

object JSONLayout {

  def toLogLevel(level: Level): LogLevel.level =
    level match {
      case Level.DEBUG => LogLevel.DEBUG
      case Level.ERROR => LogLevel.ERROR
      case Level.INFO => LogLevel.INFO
      case Level.WARN => LogLevel.WARN
      case Level.TRACE => LogLevel.TRACE
    }

  def dateFormat(timestamp: Long): String = {
     FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("UTC")).format(timestamp)
  }

}

