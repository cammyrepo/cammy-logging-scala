package com.cammy.logging.logger.impl

import java.util

import org.slf4j.Marker

import scala.collection.JavaConversions._

case class TagMarker(tags : Map[String, String]) extends Marker{

  override def hasChildren: Boolean = false

  override def getName: String = "Tags"

  override def remove(reference: Marker): Boolean = true

  override def contains(other: Marker): Boolean = false

  override def contains(name: String): Boolean = false

  override def iterator(): util.Iterator[Marker] = Iterator[Marker]()

  override def add(reference: Marker): Unit = Unit

  override def hasReferences: Boolean = false

}
