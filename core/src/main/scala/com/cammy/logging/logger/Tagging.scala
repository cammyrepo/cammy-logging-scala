package com.cammy.logging.logger

import java.util.UUID

import com.cammy.logging.domain.message.TagKey


trait Tagging {

  def withTag(tagKey : String, tagValue : String) : CammyLogger
  def withTags(keyValuePairs: (String,String)*) : CammyLogger

  def withCorrelationId(id : UUID) = withTag(TagKey.CORRELATION_ID, id.toString )
  def withAccountId(id : String) = withTag(TagKey.ACCOUNT_ID,id )
  def withCameraId(id : String) = withTag(TagKey.CAMERA_ID,id )
  def withSnapshotId(id : String) = withTag(TagKey.SNAPSHOT_ID,id )
  def withEventIdId(id : String) = withTag(TagKey.EVENT_ID,id )
  def withHomeAlarmId(id : String) = withTag(TagKey.HOME_ALARM_ID,id )
  def withNVRId(id : String) = withTag(TagKey.NVR_ID,id )
  def withIncidentId(id : String) = withTag(TagKey.INCIDENT_ID,id )
}
