package com.cammy.logging.logger.impl

import com.cammy.logging.logger.{Tagging, CammyLogger}
import com.typesafe.scalalogging.{Logger => TypeSafeLogger}


case class TaggedCammyLogger(tags : Map[String,String], logger : TypeSafeLogger) extends CammyLogger with Tagging {

   def withTag(tagKey : String, tagValue : String) = {
    TaggedCammyLogger(Map(tagKey ->tagValue) ++ tags, logger)
   }

   def withTags(keyValuePairs: (String, String)*): CammyLogger = {
      TaggedCammyLogger(keyValuePairs.toMap ++ tags, logger)
   }

   def error(message: String)  = logger.error(TagMarker(tags),message)
   def error(message: String, cause: Throwable) = logger.error(TagMarker(tags),message,cause)
   def error(message: String, args: AnyRef*) = logger.error(TagMarker(tags), message, args)

   def warn(message: String)  = logger.warn(TagMarker(tags),message)
   def warn(message: String, cause: Throwable) = logger.warn(TagMarker(tags),message,cause)
   def warn(message: String, args: AnyRef*) = logger.warn(TagMarker(tags), message, args)

   def info(message: String)  = logger.info(TagMarker(tags),message)
   def info(message: String, cause: Throwable) = logger.info(TagMarker(tags),message,cause)
   def info(message: String, args: AnyRef*) = logger.info(TagMarker(tags), message, args)

   def debug(message: String)  = logger.debug(TagMarker(tags),message)
   def debug(message: String, cause: Throwable) = logger.debug(TagMarker(tags),message,cause)
   def debug(message: String, args: AnyRef*) = logger.debug(TagMarker(tags), message, args)

   def trace(message: String)  = logger.trace(TagMarker(tags),message)
   def trace(message: String, cause: Throwable) = logger.trace(TagMarker(tags),message,cause)
   def trace(message: String, args: AnyRef*) = logger.trace(TagMarker(tags), message, args)


}
