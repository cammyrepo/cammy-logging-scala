package com.cammy.logging.logger.impl

import com.cammy.logging.logger.{Tagging, CammyLogger}
import com.typesafe.scalalogging.{Logger => TypeSafeLogger}


case class DefaultCammyLogger(val typeSafelogger : TypeSafeLogger) extends CammyLogger with Tagging{


  def withTag(tagKey : String, tagValue : String) : CammyLogger =
     TaggedCammyLogger(Map(tagKey -> tagValue), typeSafelogger)

  def withTags(keyValuePairs: (String, String)*): CammyLogger = {
    TaggedCammyLogger(keyValuePairs.toMap, typeSafelogger)
  }

  def error(message: String)  = typeSafelogger.error(message)
  def error(message: String, cause: Throwable) = typeSafelogger.error(message,cause)
  def error(message: String, args: AnyRef*) = typeSafelogger.error(message, args)

  def warn(message: String): Unit =  typeSafelogger.warn(message)
  def warn(message: String, cause: Throwable) = typeSafelogger.warn(message,cause)
  def warn(message: String, args: AnyRef*): Unit = typeSafelogger.warn(message, args)

  def info(message: String): Unit =  typeSafelogger.info(message)
  def info(message: String, cause: Throwable) = typeSafelogger.info(message,cause)
  def info(message: String, args: AnyRef*): Unit = typeSafelogger.info(message, args)

  def debug(message: String): Unit =  typeSafelogger.debug(message)
  def debug(message: String, cause: Throwable) = typeSafelogger.debug(message,cause)
  def debug(message: String, args: AnyRef*): Unit = typeSafelogger.debug(message, args)

  def trace(message: String): Unit =  typeSafelogger.trace(message)
  def trace(message: String, cause: Throwable) = typeSafelogger.trace(message,cause)
  def trace(message: String, args: AnyRef*): Unit = typeSafelogger.trace(message, args)

}
