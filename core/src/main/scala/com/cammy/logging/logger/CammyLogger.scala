package com.cammy.logging.logger

import com.cammy.logging.domain.message.ContextTagKey
import org.slf4j.MDC


trait CammyLogger {

  def error(message: String) : Unit
  def error(message: String, cause: Throwable): Unit
  def error(message: String, args: AnyRef*) : Unit

  def warn(message: String): Unit
  def warn(message: String, cause: Throwable): Unit
  def warn(message: String, args: AnyRef*): Unit

  def info(message: String): Unit
  def info(message: String, cause: Throwable): Unit
  def info(message: String, args: AnyRef*): Unit

  def debug(message: String): Unit
  def debug(message: String, cause: Throwable): Unit
  def debug(message: String, args: AnyRef*): Unit

  def trace(message: String): Unit
  def trace(message: String, cause: Throwable): Unit
  def trace(message: String, args: AnyRef*): Unit

}
