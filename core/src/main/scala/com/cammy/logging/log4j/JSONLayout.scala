package com.cammy.logging.log4j

import java.util.{UUID, TimeZone}

import com.cammy.logging.log4j.JSONLayout._
import com.cammy.logging.domain.message.ESFieldName._
import com.cammy.logging.domain.message.MetaKey._
import com.cammy.logging.domain.message.{ContextTagKey, LogException, LogLevel}
import com.cammy.logging.layout.LogMessageBuilder
import org.apache.commons.lang.StringUtils
import org.apache.commons.lang.time.FastDateFormat
import org.apache.log4j.{Layout, Level}
import org.apache.log4j.spi.LoggingEvent
import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.util.Try
import scalaz.syntax.std.boolean._


class JSONLayout() extends Layout {


  override def format(event: LoggingEvent): String = {

    val timeStamp = dateFormat(event.getTimeStamp)
    val logLevel = toLogLevel(event.getLevel)
    val message = event.getRenderedMessage

    // filter correlation Id , if present
    val (mdcCorrelationId, tags ) =  getTags(event).partition(_._1 == CORRELATION_KEY)
    val correlationId = mdcCorrelationId.get(CORRELATION_KEY).flatMap( id => Try { UUID.fromString(id) }.toOption )
    val meta = getMeta(event)
    val exception = getExceptionInfo(event)

    val dynamicFieldBuilder = LogMessageBuilder.sessionBuilder(correlationId, tags, meta)
    dynamicFieldBuilder(message, Option(logLevel), timeStamp, exception, Map())
  }

  // seesion tags from MDC map
  def getTags(event: LoggingEvent) = {

    val mdc = event.getProperties
    val contextTags = mdc.collect{ case (ContextTagKey(key) , value ) => (key, value.toString)}

    contextTags.toMap[String, String]
  }

  def getMeta(event: LoggingEvent) = {

    val threadMeta = (THREAD_NAME , event.getThreadName)

    val locationInfoOpt = for {
      shouldInclude <- includeLocationInfo.option("locationInfo")
      callerData <- Some(event.getLocationInformation)
    } yield callerData

    val meta = locationInfoOpt match {
      case Some(locationInfo)  => List((FILE_NAME, locationInfo.getFileName) ,
        (LINE_NUMBER, locationInfo.getLineNumber),
        (CLASS_NAME, locationInfo.getClassName),
        (METHOD_NAME, locationInfo.getMethodName)) ++ List(threadMeta)
      case None => List(threadMeta)
    }

    meta.toMap
  }

  def getExceptionInfo(logEvent: LoggingEvent) = {

    for {
      ignore  <- includeThrowableInfo.option("ignore")
      throwableInfo <- Option(logEvent.getThrowableInformation)
      throwable <- Option(throwableInfo.getThrowable)
      exMsg <- Option(throwable.getMessage)
      stackTrace = Option(StringUtils.join(throwableInfo.getThrowableStrRep.toList, "\n"))
      className = Option(throwable.getClass.getCanonicalName)
    } yield LogException(className, Option(exMsg), stackTrace)
  }

  @BeanProperty
  var includeLocationInfo: Boolean = false
  @BeanProperty
  var includeThrowableInfo = true

  override def ignoresThrowable(): Boolean = includeThrowableInfo
  override def activateOptions(): Unit = {}
}

object JSONLayout {

  def toLogLevel(level: Level): LogLevel.level =
    level match {
      case Level.DEBUG => LogLevel.DEBUG
      case Level.ERROR => LogLevel.ERROR
      case Level.INFO => LogLevel.INFO
      case Level.WARN => LogLevel.WARN
      case Level.TRACE => LogLevel.TRACE
    }

  def dateFormat(timestamp: Long): String = {
    FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("UTC")).format(timestamp)
  }

}


