package com.cammy.logging.trace

import com.cammy.logging.domain.message.ContextTagKey
import com.cammy.logging.trace.Implicits._
import org.scalatest.mock.MockitoSugar
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FunSpec, GivenWhenThen, Matchers}
import org.slf4j.MDC


class ImplicitsTest  extends FunSpec with GeneratorDrivenPropertyChecks with GivenWhenThen with MockitoSugar with Matchers {


  describe("with context") {

    describe("#log message") {

      it("should include context tag") {

        When("doing logger.withCtx")
        withLoggingContext("tag" -> "value") {
          "value" should equal(MDC.get(ContextTagKey("tag")))
        }

      }
    }
  }
}
