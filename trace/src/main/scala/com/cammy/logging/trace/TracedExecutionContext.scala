package com.cammy.logging.trace

import scala.concurrent.ExecutionContext


/**
  * Wrapper around an existing ExecutionContext that makes it propagate MDC information.
  */
class TracedExecutionContext(wrapped: ExecutionContext)
  extends ExecutionContext with MDCPropagatingExecutionContext {

  override def execute(r: Runnable): Unit = wrapped.execute(r)

  override def reportFailure(t: Throwable): Unit = wrapped.reportFailure(t)
}

object TracedExecutionContext {
  def apply(wrapped: ExecutionContext): TracedExecutionContext = {
    new TracedExecutionContext(wrapped)
  }
}
