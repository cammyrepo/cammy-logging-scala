package com.cammy.logging.trace

import com.cammy.logging.domain.message.{TagKey, ContextTagKey}
import com.cammy.logging.domain.trace.Constants
import com.stackmob.newman.dsl.Builder
import org.http4s.Request
import org.slf4j.MDC
import scala.concurrent.ExecutionContext


object Implicits {

  implicit lazy val global = TracedExecutionContext(ExecutionContext.Implicits.global)

  def withLoggingContext[A](keyValuePairs: (String, String)*)(f: => A): A = {
    val old = for {(key, value) <- keyValuePairs.toMap} yield {
      val tmp = (key, Option(MDC.get(key)))
      MDC.put(ContextTagKey(key), value)
      tmp
    }
    try {
      f
    } finally {
      for {(key, oldValue) <- old.toMap} {
        oldValue match {
          case None => MDC.remove(ContextTagKey(key))
          case Some(value) => MDC.put(key, value)
        }
      }
    }
  }

  def addTraceHeader(requestBuilder: Builder) = {

    val correlationId = Option(MDC.get(ContextTagKey(TagKey.CORRELATION_ID)))
    correlationId.fold(requestBuilder) { corrId => requestBuilder.addHeaders((Constants.CORRELATION_ID_HEADER, corrId)) }

  }

  def retrieveTraceHeader(req: Request) = {

    req.headers.get(`Correlation-Id`) match {
      case Some(corrId) => MDC.put(ContextTagKey(TagKey.CORRELATION_ID), corrId.corrId)
      case None =>
    }
  }


}
