package com.cammy.logging.trace

import com.cammy.logging.domain.trace.Constants
import org.http4s._
import org.http4s.util.Writer
import scala.util.Try
import scalaz.Scalaz._
import org.http4s.dsl._

final case class `Correlation-Id`(corrId: String) extends Header.Parsed {
  override def key = `Correlation-Id`
  override def renderValue(writer: Writer): writer.type =
    writer.append(corrId)
}

object `Correlation-Id` extends HeaderKey.Singleton {
  type HeaderT = `Correlation-Id`

  def name = Constants.CORRELATION_ID_HEADER.ci

  def matchHeader(header: Header): Option[`Correlation-Id`] = {
    if (header.name == name)
      Try(`Correlation-Id`(header.value)).toOption
    else None
  }

  def parse(s: String): ParseResult[`Correlation-Id`] = {
    `Correlation-Id`(s).right
  }
}
