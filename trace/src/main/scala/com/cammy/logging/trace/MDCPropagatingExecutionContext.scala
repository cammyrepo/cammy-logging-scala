package com.cammy.logging.trace

import org.slf4j.MDC
import scala.concurrent.ExecutionContext

/*
 * based on http://code.hootsuite.com/logging-contextual-info-in-an-asynchronous-scala-application/
 */
trait MDCPropagatingExecutionContext extends ExecutionContext {
  // name the self-type "self" so we can refer to it inside the nested class
  self =>

  override def prepare(): ExecutionContext = new ExecutionContext {
    // Save the call-site MDC state
    val context = MDC.getCopyOfContextMap

    def execute(r: Runnable): Unit = self.execute(new Runnable {
      def run(): Unit = {
        // Save the existing execution-site MDC state
        val oldContext = MDC.getCopyOfContextMap
        try {
          // Set the call-site MDC state into the execution-site MDC
          if (context != null )
            MDC.setContextMap(context)
          else
            MDC.clear()

          r.run()
        } finally {
          // Restore the existing execution-site MDC state
          if (oldContext != null)
            MDC.setContextMap(oldContext)
          else
            MDC.clear()
        }
      }
    })

    def reportFailure(t: Throwable): Unit = self.reportFailure(t)
  }
}

