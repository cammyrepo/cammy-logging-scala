-------

Logging lib to send logs to elasticsearch for JVM based lang( scala, java)
==============================

cammy-logging currently supports logging using following logging libraries:

* [Log4J](https://github.com/logentries/le_java/wiki/Log4j)
* [Logback](https://github.com/logentries/le_java/wiki/Logback)


-------

Including it in your project
=============
```scala
libraryDependencies ++= Seq(
                            "com.cammy"                   %% "logging-core"                % "0.1.10-SNAPSHOT" withSources(),
                            "com.cammy"                   %% "logging-trace"               % "0.0.3-SNAPSHOT" withSources(),
                            "ch.qos.logback"              %  "logback-classic"             % "1.1.6"
                          )
 ```
                          
`logging-trace` package is used to pass correlation id around service, threads and queues.

-------

Usage
==================

`cammy-logging` uses aws kinesis firehose to transport logs. You will need credentials and stream name to use  it.

logback.xml
```xml
    <appender name="CAMMY" class="com.cammy.logging.logback.CammyAppender">

        <layout class="com.cammy.logging.logback.CammyLogLayout"/>

        <param name="StreamName" value="${LOGGER_DELIVERY_STREAM}"/>
        <param name="Key" value="${LOGGER_KEY}"/>
        <param name="Secret" value="${LOGGER_SECRET}"/>
        <param name="Region" value="us-east-1"/>
        <param name="debug" value="false" />"

    </appender>
```    

The CammyLogging traits from the com.cammy.logging.CammyLogging package define the logger member as a lazy. It deletgates logger 
to typesafe LazyLogging, which internally creates underlying SLF4J logger.

```scala
 class MyClass extends CammyLogging {
  logger.debug("This is customised logger ;-)")
 }
``` 

You can tag log mesaage using logger intialized using CammyLogging

```
logger.withTag("tagKey", "tagValue').info("log message")
```

[Please see wiki for more documentation and log format](https://cammydev.atlassian.net/wiki/display/BAC1/Logging)

-------
