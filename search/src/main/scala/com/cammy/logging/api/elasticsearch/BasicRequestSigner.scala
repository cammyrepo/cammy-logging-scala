package com.cammy.logging.api.elasticsearch

import java.nio.charset.StandardCharsets
import java.util.Base64
import spray.http._
import com.sumologic.elasticsearch.restlastic.RequestSigner
import spray.http.HttpHeaders.RawHeader
import spray.http.HttpRequest


case class BasicRequestSigner( userName : String, password : String) extends RequestSigner  {
  override def withAuthHeader(httpRequest: HttpRequest): HttpRequest = {
    val headerValue = Base64.getEncoder.encode((userName + ":" + password).getBytes(StandardCharsets.UTF_8))
    httpRequest.withHeaders(RawHeader("Authorization","Basic " +  new String(headerValue, StandardCharsets.UTF_8)))
  }
}
