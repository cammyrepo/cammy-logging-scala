package com.cammy.logging.api

import java.util.concurrent.Executors

import com.cammy.logging.api.config.Configuration
import com.cammy.logging.api.controller.{DBSyncService, HealthHttpService, LogSearchHttpService}
import com.cammy.logging.api.elasticsearch.BasicRequestSigner
import com.cammy.logging.api.repository.{ESCameraActivityRespository, ESCameraMetaDataRepository}
import com.cammy.logging.api.service.CameraLogSearchService
import com.cammy.metrics.MetricsCollectorImpl
import com.cammy.util.http4s.middleware.{HttpResponseLogging, Metrics}
import com.sumologic.elasticsearch.restlastic.{Endpoint, RestlasticSearchClient, StaticEndpoint}
import org.http4s._
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.server.middleware.AutoSlash
import org.log4s._
import scala.concurrent.ExecutionContext

object Boot {

  private[this] val logger = getLogger

  def main(args: Array[String]): Unit = {

    implicit val es = Executors.newFixedThreadPool(Configuration.httpConfig.threadCount)
    implicit val ec = ExecutionContext.fromExecutor(es)

    val metrics = new MetricsCollectorImpl(Configuration.metricsConfig)
    //TODO client catch exception
    val elasticClient = new RestlasticSearchClient(new StaticEndpoint(new Endpoint( Configuration.elasticConfig.url, Configuration.elasticConfig.port)),
                                                   Some(BasicRequestSigner(Configuration.elasticConfig.accessKey, Configuration.elasticConfig.secretKey )) )
    val cameraRespository = ESCameraActivityRespository(elasticClient)
    val camreaMetaDataRespository = ESCameraMetaDataRepository(elasticClient)

    val healthService = HealthHttpService(elasticClient)
    val cameraLogSearchService = CameraLogSearchService(cameraRespository, camreaMetaDataRespository)
    val logsearchService = LogSearchHttpService(cameraLogSearchService)
    val mongoSyncService = DBSyncService(metrics)

    val backendService: HttpService = Router(
      "/health"  -> Metrics.meter(metrics, "health")(healthService.service),
      "/search" -> Metrics.meter(metrics, "search")(logsearchService.service)
  //    "/sync" -> Metrics.meter(metrics, "sync")(mongoSyncService.service)
    )

    val withLogging   = HttpResponseLogging(l => logger.info(l))(backendService)
    val withAutoSlash = AutoSlash(withLogging)

    val config = Configuration.httpConfig
    logger.info("Starting logging service in mode: " + Configuration.env)
    logger.info(s"[info] Starting LogSearchService on ${config.host}:${config.port}")

    val server: BlazeBuilder = BlazeBuilder.bindHttp(config.port, config.host)
      .mountService(withAutoSlash, "/")
      .withConnectorPoolSize(50)
      .withServiceExecutor(es)
      .enableHttp2(true)
      .withNio2(true)

    server.run
  }

}
