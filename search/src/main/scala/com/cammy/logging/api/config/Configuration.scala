package com.cammy.logging.api.config

import com.typesafe.config.{Config, ConfigFactory}
import com.cammy.metrics.MetricsCollectorConfiguration

import scala.util.Try
import scala.concurrent.duration._



object Configuration {




  case class HttpConfig(host: String, port: Int, threadCount : Int)
  case class ElasticConfig(accessKey: String, secretKey: String, region: String, url: String, port: Int)
  case class MongoDbConfiguration(uri: String, database: String, accountsCollection: String, camerasCollection: String,
                                  timeout: Option[FiniteDuration] = None)

  val env = Option(System.getenv("SCALA_ENV")).getOrElse("development")
  val envConfig: Config = ConfigFactory.load().getConfig("environment").getConfig(env)

  lazy val httpConfig = {
    val config = envConfig.getConfig("http")
    HttpConfig(
      host = config.getString("host"),
      port = config.getInt("port"),
      threadCount = Try(config.getInt("threadCount")).getOrElse(10)
    )
  }

  lazy val elasticConfig = {
    val config = envConfig.getConfig("elastic")
    ElasticConfig(
      accessKey = config.getString("access_key_id"),
      secretKey = config.getString("access_key_secret"),
      region = config.getString("region"),
      port = config.getInt("port"),
      url = config.getString("endpoint")
    )
  }


  lazy val mongoDbConfig = {
    val config = envConfig.getConfig("mongodb")
    MongoDbConfiguration(
      config.getString("uri"),
      config.getString("database"),
      config.getString("events_collection"),
      config.getString("cameras_collection"),
      timeout = Try(config.getInt("timeout")).toOption.map(_.millis)
    )
  }

  lazy val metricsConfig = {
    val config = envConfig.getConfig("metrics")
    MetricsCollectorConfiguration(
      Try(config.getString("prefix")).getOrElse("logging"),
      Try(config.getString("hostname")).getOrElse("localhost"),
      Try(config.getInt("port")).getOrElse(8125)
    )
  }

}