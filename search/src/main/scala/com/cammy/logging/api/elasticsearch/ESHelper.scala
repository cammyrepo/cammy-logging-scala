package com.cammy.logging.api.elasticsearch


import com.sumologic.elasticsearch.restlastic.dsl.Dsl._
import scala.language.implicitConversions

object ESHelper {

  val ES_LOGGING_INDEX_PATTERN  = "log*"
  val ES_FIREHOSE_TYPE = "firehose"
  val TIME_STAMP_FIELD = "@timestamp"
  val CAMERA_ID_FIELD = "camera_id"
  val MESSAGE_FIELD = "message"
  val FIREHOSE_SUCESSFUL_LOGING_MESSAGE = "Login success"
  val ES_MONGO_INDEX_PATTERN  = "mongodb"
  val ES_CAMERAS_TYPE = "cameras"


  implicit def toFilter(query: Query) : Filter  = QueryFilter(query)

  case class QueryFilter(query: Query) extends Filter {
    override def toJson: Map[String, Any] = {
      query.toJson
    }
  }
}
