package com.cammy.logging.api.repository

import com.cammy.logging.domain.search.SearchParam
import com.cammy.logging.domain.error.SearchError
import com.cammy.logging.domain.search.SearchInterval

import scalaz.\/
import scalaz.concurrent.Task

trait CameraActivityRepository {

  def getActiveCameras(searchInterval: SearchInterval, limit: Int): Task[SearchError \/Set[String]]
}
