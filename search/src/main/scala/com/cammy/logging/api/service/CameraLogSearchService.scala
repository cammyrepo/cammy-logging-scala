package com.cammy.logging.api.service

import com.cammy.logging.api.repository.{CameraActivityRepository, CameraMetaDataRespository}
import com.cammy.logging.domain._
import com.cammy.logging.domain.error.SearchError
import com.cammy.logging.domain
import com.typesafe.scalalogging.LazyLogging
import scalaz.concurrent.Task
import scalaz.{EitherT, \/}

case class CameraLogSearchService(
    cameraActivityRepository: CameraActivityRepository,
    cameraMetaDataRespository: CameraMetaDataRespository)
    extends LazyLogging {

  def getAllCameras = cameraMetaDataRespository.getAllCameras
  def getActiveCameras(searchInterval: domain.search.SearchInterval)  = filterCameras(searchInterval, domain.camera.Active)
  def getInActiveCameras(searchInterval: domain.search.SearchInterval)  = filterCameras(searchInterval, domain.camera.Inactive)

  private def filterCameras(searchInterval: domain.search.SearchInterval,
                            status: domain.camera.CameraStatus): Task[\/[SearchError, List[domain.camera.Camera]]] = {

    val result = for {
      allCameras <- EitherT(cameraMetaDataRespository.getAllCameras)
      activeCameras <- EitherT(cameraActivityRepository.getActiveCameras(searchInterval, allCameras.size))
    } yield {
      val filteredCameras = status match {
        case domain.camera.Inactive =>
          allCameras.filterNot(camera => activeCameras.contains(camera.id))
        case domain.camera.Active =>
          allCameras.filter(camera => activeCameras.contains(camera.id))
        case _ => allCameras
      }
      logger.debug(
          s"Cameras - all : ${allCameras.size}  active : ${activeCameras.size}  filtered ${filteredCameras.size}")
      filteredCameras.toList
    }
    result.run
  }
}
