package com.cammy.logging.api.controller

import com.cammy.metrics.MetricsCollector
import com.typesafe.scalalogging.LazyLogging
import org.http4s.dsl._
import org.http4s.{HttpService, Response}

import scala.sys.process.Process
import scalaz.concurrent.Task

case class DBSyncService(metrics: MetricsCollector) extends LazyLogging {

  def service = HttpService {
    case req@GET -> Root / "start" => start
    case req@GET -> Root / "stop" => stop
  }

  val cmd = s"transporter run --config mongo-elastic-sync/config.yml mongo-elastic-sync/application.js 2>&1 &"

  def start: Task[Response] =
    try {
       Process(Seq("sh", "-c", cmd)).run
       Ok(" transporter started")
       } catch {
      case e: Exception =>
        logger.error(e.getMessage, e)
        InternalServerError("Could not start transporter")
    }

  def stop =
    try {

      val res = Process("ps x").!!
      val pidOpt =
        res.split("\n")
          .find(s => s.contains("transporter"))
          .map { l => logger.info("Process found: " + l); l }
          .flatMap { _.trim.split("\\s+").headOption.map(_.toLong) }

      pidOpt.fold(InternalServerError("Could not stop transporter")) {
        pid => Process(s"kill -9 $pid").run.exitValue()match {
          case 0 => Ok(" transporter started")
          case _ => InternalServerError("Could not stop transporter")
        }}

    } catch {
      case e: Exception =>
        logger.error(e.getMessage, e)
        InternalServerError("Could not stop transporter")
    }

}
