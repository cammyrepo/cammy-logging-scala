package com.cammy.logging.api.repository

import com.cammy.logging.api.config.Configuration.MongoDbConfiguration
import com.cammy.logging.domain.camera.Camera
import Camera._
import com.cammy.logging.domain.error.{ESMongoSearchError, SearchError}
import com.cammy.logging.domain.camera.Camera
import com.mongodb.casbah.{MongoClient, MongoClientURI, MongoCollection}
import com.sumologic.elasticsearch.restlastic.RestlasticSearchClient.ReturnTypes.SearchResponse
import com.sumologic.elasticsearch.restlastic.dsl.Dsl._
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scalaz.concurrent.Task
import scalaz.{\/, _}


case class MongoCameraMetaDataRepository(mongoConfiguration :MongoDbConfiguration) extends CameraMetaDataRespository with LazyLogging{

  val mongoDB  = MongoClient(MongoClientURI(mongoConfiguration.uri))(mongoConfiguration.database)

  val accounts: MongoCollection = mongoDB("cameras")

  override def getAllCameras: Task[SearchError \/ Set[Camera]] = ???

}
