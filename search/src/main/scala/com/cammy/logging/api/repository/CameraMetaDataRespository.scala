package com.cammy.logging.api.repository

import com.cammy.logging.domain.error.{SearchError, ESLogSearchError}
import com.cammy.logging.domain.camera.Camera

import scalaz.\/
import scalaz.concurrent.Task


trait CameraMetaDataRespository {

  def getAllCameras : Task[SearchError \/ Set[Camera]]

}
