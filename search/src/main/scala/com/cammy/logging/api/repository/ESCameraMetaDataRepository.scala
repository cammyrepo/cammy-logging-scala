package com.cammy.logging.api.repository

import com.cammy.logging.domain.camera.Camera
import Camera._
import com.cammy.logging.api.elasticsearch.ESHelper._
import com.cammy.logging.domain.error.{SearchError, ESMongoSearchError, ESLogSearchError}
import com.cammy.logging.domain.camera.Camera
import com.cammy.util.FutureHelpers._
import com.sumologic.elasticsearch.restlastic.RestlasticSearchClient
import com.sumologic.elasticsearch.restlastic.RestlasticSearchClient.ReturnTypes.SearchResponse
import com.sumologic.elasticsearch.restlastic.dsl.Dsl._
import com.sumologic.elasticsearch.restlastic.dsl.Dsl
import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.ExecutionContext.Implicits.global
import scalaz.{\/, _}
import scalaz.concurrent.Task


case class ESCameraMetaDataRepository(elasticClient : RestlasticSearchClient) extends CameraMetaDataRespository with LazyLogging{

  val mongoIndex = Dsl.Index(ES_MONGO_INDEX_PATTERN)
  val cameratype = Type(ES_CAMERAS_TYPE)

  override def getAllCameras: Task[SearchError \/ Set[Camera]] = {

    //TODO use scroll request
    val result = for {
     count  <- elasticClient.count(mongoIndex, cameratype, QueryRoot(MatchAll))
     searchResult  <- elasticClient.query(mongoIndex, cameratype, QueryRoot(query = MatchAll, sizeOpt = Some(count)))
    }  yield searchResult match {
        case SearchResponse.empty => -\/(ESMongoSearchError("No camera found in ES mongo index"))
        case res =>
          val cameras = res.sourceAsMap.map(fromMap)
          logger.debug("Response : got " + cameras.size + " cameras")
          \/-(cameras.toSet)
      }
    result.toTask
  }

}
