package com.cammy.logging.api.controller

import com.cammy.logging.api.config.Configuration
import com.cammy.metrics.MetricsCollector
import com.cammy.util.FutureHelpers._
import com.sumologic.elasticsearch.restlastic.dsl.Dsl._
import com.sumologic.elasticsearch.restlastic.{Endpoint, RestlasticSearchClient, StaticEndpoint}
import com.typesafe.scalalogging.LazyLogging
import org.http4s.HttpService
import org.http4s.dsl._

import scala.concurrent.ExecutionContext.Implicits.global

case class HealthHttpService(elasticClient: RestlasticSearchClient) extends LazyLogging {

  def service = HttpService { case req@GET -> Root => {

   // val elasticClient = new RestlasticSearchClient(new StaticEndpoint(new Endpoint(Configuration.elasticConfig.url, Configuration.elasticConfig.port)))
    val futureResult = elasticClient.query(
     Index("mongodb"), Type("cameras"),
     QueryRoot(query = MatchAll, timeout = Some(5), sizeOpt = Some(1) )).toTask

    futureResult flatMap { res => res.length match {
      case 0 => InternalServerError("failed")
      case _ => Ok("OK")
    }
    }
  }
  }
}
