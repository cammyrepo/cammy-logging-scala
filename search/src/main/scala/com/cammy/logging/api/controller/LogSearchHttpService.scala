package com.cammy.logging.api.controller

import com.cammy.logging.api.config.Configuration._
import com.cammy.logging.api.controller.HttpHelper._
import com.cammy.logging.api.service.CameraLogSearchService
import com.typesafe.scalalogging.LazyLogging
import org.http4s.HttpService
import org.http4s.dsl._

import scalaz.{-\/, \/-}

case class LogSearchHttpService(cameraLogService: CameraLogSearchService) extends LazyLogging {

  val DEFAULT_DAYS_TO_SEARCH = 30

   def service: HttpService = HttpService {

    case req @ GET -> Root / "cameras" =>
      cameraLogService.getAllCameras flatMap {
        case \/-(allCameras) => Ok(SuccessResponse(allCameras.toList))
        case -\/(err) =>
          InternalServerError(ErrorResponse(ApplicationError(err.reason)))
      }

    case req @ GET -> Root / "cameras" / "inactive" :? NoOfDaysParamMatcher(days) =>
      validate[Int](NoOfDaysParamMatcher, days, DEFAULT_DAYS_TO_SEARCH)
        .fold((errors) => {
          BadRequest(
            ErrorResponse(InvalidRequest(errors.map(_.reason).toString())))
        }, (searchInterval) => {
          cameraLogService.getInActiveCameras(searchInterval) flatMap {
            case \/-(cameras) => Ok(SuccessCSVResponse(cameras.toList))
            case -\/(err) =>
              InternalServerError(ErrorResponse(ApplicationError(err.reason)))
          }
        })

    case req @ GET -> Root / "cameras" / "active" :? NoOfDaysParamMatcher(days) =>
      validate[Int](NoOfDaysParamMatcher, days, DEFAULT_DAYS_TO_SEARCH)
        .fold((errors) => {
          BadRequest(
            ErrorResponse(InvalidRequest(errors.map(_.reason).toString())))
        }, (searchInterval) => {
          cameraLogService.getActiveCameras(searchInterval) flatMap {
            case \/-(cameras) => Ok(SuccessCSVResponse(cameras.toList))
            case -\/(err) =>
              InternalServerError(ErrorResponse(ApplicationError(err.reason)))
          }
        })
  }
}
