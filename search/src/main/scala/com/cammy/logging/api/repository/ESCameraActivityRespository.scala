package com.cammy.logging.api.repository

import com.cammy.logging.api.elasticsearch.ESHelper._
import com.cammy.logging.domain.error.{ESLogSearchError, SearchError}
import com.cammy.logging.domain.search.SearchInterval
import com.cammy.util.FutureHelpers._
import com.sumologic.elasticsearch.restlastic.RestlasticSearchClient
import com.sumologic.elasticsearch.restlastic.RestlasticSearchClient.ReturnTypes.Bucket
import com.sumologic.elasticsearch.restlastic.dsl.Dsl._
import com.typesafe.scalalogging.LazyLogging
import scala.concurrent.ExecutionContext.Implicits.global
import scalaz.concurrent.Task
import scalaz.{-\/, \/, \/-}


case class ESCameraActivityRespository(elasticClient: RestlasticSearchClient) extends CameraActivityRepository with LazyLogging{

  val index = Index(ES_LOGGING_INDEX_PATTERN)
  val tpe = Type(ES_FIREHOSE_TYPE)

  def getActiveCameras(searchInterval: SearchInterval, limit : Int): Task[SearchError \/ Set[String]] = {

    val rangeQuery = RangeQuery(TIME_STAMP_FIELD, Gte(searchInterval.start.get.toString), Lte(searchInterval.end.get.toString))
    val termAggregation = TermsAggregation(field = CAMERA_ID_FIELD, size = Some(limit), shardSize = None, include = None)
    val matchQuery = FilteredQuery(rangeQuery, MatchQuery(MESSAGE_FIELD, FIREHOSE_SUCESSFUL_LOGING_MESSAGE))
    val aggreQuery = AggregationQuery(matchQuery,termAggregation)


     elasticClient.bucketAggregation(index, tpe, aggreQuery)
      .map(_.buckets match {
          case Nil =>
            -\/(ESLogSearchError("No camera with "  + FIREHOSE_SUCESSFUL_LOGING_MESSAGE + " message found in ES log"))
          case buckets: List[Bucket] =>
            logger.debug("Response : got " + buckets.size  + " buckets " + buckets)
            \/-(buckets.map(m => m.key.asInstanceOf[String].toUpperCase).toSet)
        }).toTask

  }

}
