package com.cammy.logging.api.controller

import java.io.ByteArrayOutputStream

import argonaut.Argonaut._
import argonaut.{Argonaut, CursorHistory, EncodeJson}
import com.cammy.logging.domain.error.{InvalidSearchLimitError, InvalidSearchParamError, SearchError}
import com.cammy.logging.domain.camera.Camera
import com.cammy.logging.domain.search.SearchInterval
import com.github.tototoshi.csv.CSVWriter
import org.http4s.dsl.OptionalQueryParamDecoderMatcher
import org.http4s.headers.`Content-Type`
import org.http4s.{Charset, EntityEncoder, MediaType}
import org.joda.time.{DateTime, DateTimeZone}

import scalaz.Scalaz._
import scalaz._

object HttpHelper {

    val CSV_REPORT_HEADER = Seq("camera_id", "owner_email","creation_date", "last_event_date")

    sealed trait ApiError {
      def errorType: String
      def detail: String
    }

    case class UnableToDecodeJson(cursorHistory: CursorHistory) extends ApiError {
      def errorType = "UnableToDecodeJson"
      def detail = s"Can't decode JSON: ${cursorHistory.toString}"
    }

    case class InvalidRequest(message: String) extends ApiError {
      def errorType = "BadRequest"
      def detail = message
    }

    case class ApplicationError(message: String) extends ApiError {
      def errorType = "InternalServerError"
      def detail = message
    }


    sealed trait Meta
    case class SuccessMeta( objectId: Option[String] = None) extends Meta
    case class ErrorMeta(error: ApiError) extends Meta

    sealed trait ApiResponse {
      def meta: Meta
    }

    case class SuccessResponse(cameras : List[Camera], meta: SuccessMeta = SuccessMeta()) extends ApiResponse
    case class ErrorResponse(error: ApiError) extends ApiResponse {def meta: Meta = ErrorMeta(error)}
    case class SuccessCSVResponse( cameras : List[Camera], meta : SuccessMeta = SuccessMeta()) extends ApiResponse

    implicit def SuccessMetaCodecJson: EncodeJson[SuccessMeta] =
      EncodeJson((m: SuccessMeta) => m.objectId.map("object_id" := _) ->?: jEmptyObject)

    implicit def ErrorMetaCodecJson: EncodeJson[ErrorMeta] =
      EncodeJson((e: ErrorMeta) =>
        ("error_type" := e.error.errorType) ->: ("error_detail" := e.error.detail) ->: jEmptyObject)

    implicit def MetaEncodeJson: EncodeJson[Meta] = EncodeJson((e: Meta) => e match {
      case e: SuccessMeta => e.asJson
      case e: ErrorMeta => e.asJson
    })

    implicit def SuccessResponseEncodeJson: EncodeJson[SuccessResponse] =
      jencode2L((p: SuccessResponse) => (p.cameras, p.meta))("response", "meta")

    implicit def ErrorResponseEncodeJson: EncodeJson[ErrorResponse] =
      EncodeJson((e: ErrorResponse) =>
        ("meta" := e.meta.asJson) ->: ("response" := jNull) ->: jEmptyObject)

    implicit def SuccessResponseEncoder: EntityEncoder[SuccessResponse] =
      EntityEncoder.stringEncoder(Charset.`UTF-8`).contramap[SuccessResponse] { successResponse: SuccessResponse =>
        Argonaut.nospace.pretty(successResponse.asJson)
      }.withContentType(`Content-Type`(MediaType.`application/json`))

    implicit def ErrorResponseEncoder: EntityEncoder[ErrorResponse] =
      EntityEncoder.stringEncoder(Charset.`UTF-8`).contramap[ErrorResponse] { errorResponse =>
        Argonaut.nospace.pretty(errorResponse.asJson)
      }.withContentType(`Content-Type`(MediaType.`application/json`))

    implicit def SuccessCSVResponseEncoder: EntityEncoder[SuccessCSVResponse] =
      EntityEncoder.byteArrayEncoder.contramap[SuccessCSVResponse] { successCSVResponse: SuccessCSVResponse =>
        val  bos = new ByteArrayOutputStream()
        val writer = CSVWriter.open(bos)
        writer.writeRow(CSV_REPORT_HEADER)
        successCSVResponse.cameras.foreach(camera => CSVWriter.open(bos).writeRow(camera))
        bos.toByteArray
      }.withContentType(`Content-Type`(MediaType.`text/csv`))

//      implicit def SuccessCSVResponseEncoder: EntityEncoder[SuccessCSVResponse] =
//        EntityEncoder.fileEncoder.contramap[SuccessCSVResponse] { successCSVResponse: SuccessCSVResponse =>
//          val  bos =  new File("/tmp/1")
//          successCSVResponse.cameras.foreach(camera => CSVWriter.open(bos).writeRow(camera))
//          bos
//        }.headers.put(`Content-Disposition`("inline", Map("filename", "test.csv")))


  def validate[T](paramMatcher : OptionalQueryParamDecoderMatcher[T], value : Option[T], default: T): ValidationNel[SearchError, SearchInterval] = {

    paramMatcher match {

      case NoOfDaysParamMatcher => {

        val days = value.getOrElse(default).asInstanceOf[Int]
        val end   = DateTime.now(DateTimeZone.UTC).getMillis
        val start   = DateTime.now(DateTimeZone.UTC).minusDays(days).getMillis
        val searchInterval = SearchInterval(Some(start), Some(end))
        if (days <= 0 || days > 90 ) InvalidSearchLimitError("number of days to search must between 0 and 90").failureNel
        else {
          searchInterval.successNel
        }

      }
      case _ => InvalidSearchParamError("Invalid search parameter").failureNel
    }

  }

  object NoOfDaysParamMatcher extends OptionalQueryParamDecoderMatcher[Int]("days")


}
