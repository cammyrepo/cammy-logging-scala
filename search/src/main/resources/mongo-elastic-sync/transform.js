var map={};
var db = "cammy-api-prod"
//var db = "cammy-api-staging"

module.exports = function(msg) {
 if(msg.ns == db + ".cameras"){
  //console.log(JSON.stringify(msg,null,' '))
  console.log(msg.ns + "(" +  map[msg.ns] + ") " + msg.op + "  : " + msg.data._id)
  msg.data.camera_id = msg.data._id
  delete msg.data._id
  delete msg.data.ftp
  if (!(typeof  msg.data.location === 'undefined' )){
     msg.data.location.lon = msg.data.location.lng
     delete msg.data.location.lng
  }
 } else {
  console.log(msg.ns + "(" +  map[msg.ns] + ") " + msg.op + "  : " + msg.data._id.$oid )
 }
 if( typeof map[msg.ns] === 'undefined' )
     map[msg.ns] = 1;
  else
     map[msg.ns] = map[msg.ns] + 1;

  return msg
}