package com.cammy.logging.api.repository

import com.cammy.logging.api.config.Configuration
import com.cammy.logging.domain.search.SearchInterval
import com.sumologic.elasticsearch.restlastic.{RestlasticSearchClient, StaticEndpoint, Endpoint}
import com.typesafe.scalalogging.LazyLogging
import org.joda.time.{DateTimeZone, DateTime}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.Span
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}
import com.sumologic.elasticsearch_test.ElasticsearchIntegrationTest
import com.sumologic.elasticsearch.restlastic.dsl.Dsl._
import scala.concurrent.duration._
import org.scalatest.time.{Millis, Seconds, Span}
import com.cammy.logging.api.elasticsearch.ESHelper._
import scala.concurrent.Await

class CameraActivityRepositoryTest
    extends WordSpec
    with Matchers
    with ScalaFutures
    with BeforeAndAfterAll
    with ElasticsearchIntegrationTest
    with LazyLogging{

  implicit val patience = PatienceConfig(timeout = scaled(Span(10, Seconds)), interval = scaled(Span(50, Millis)))

  val index = Index("logs-2016-10-23")
  val tpe = Type(ES_FIREHOSE_TYPE)

  val logRecord = Document(
      "log_record_1",
      Map("appVersion" -> "0.0.9",
          "@timestamp" -> "2016-10-23T11:57:13.720Z",
          "source_host" -> "ip-172-31-2-37",
          "level" -> "INFO",
          "appName" -> "cammyFTP",
          "thread_name" -> "pool-8-thread-3",
          "camera_id" -> "1NJHA4",
          "correlation_id" -> "924216fa-6c3e-491a-a295-af468f18bc4e",
          "remote_address" -> "/61.69.81.102:43366",
          "logger_name" -> "org.apache.ftpserver.command.impl.PASS",
          "message" -> "Login failure - 1NJHA4"))

  val mongoDBRecord = Document(
      "camera_record_1",
      Map(
          "camera_id" -> "1NJFKS",
          "creator" -> "test_acct_728@example.com",
          "creator_id" -> "55cc2caebebe2503000f632b",
          "date_created" -> 1439444146,
          "last_event" -> Map(
              "_id" -> "55cc2cb74774341a1b2baec0",
              "start_timestamp" -> 1439444151.364
          ),
          "local_ip" -> "192.168.1.232",
          "local_port" -> "8001",
          "mac_address" -> "12-34-56-78-9A-BC",
          "manufacturer" -> "D-Link International Inc.",
          "model" -> "DCS-932L",
          "motion_enabled" -> false,
          "name" -> "Test Camera",
          "owner" -> "test_acct_728@example.com",
          "owner_id" -> "55cc2caebebe2503000f632b",
          "public" -> false,
          "stream_url" -> "rtsp://{user}:{pass}@{host}/video.sdp",
          "timezone" -> "Australia/Sydney"
      ))

  lazy val restClient = {
    val (host, port) = endpoint
    new RestlasticSearchClient(new StaticEndpoint(new Endpoint(host, port)))
  }

  lazy val cameraActivityRepo = ESCameraActivityRespository(restClient)

  private def refreshWithClient(): Unit = {
    Await.result(restClient.refresh(index), 2.seconds)
  }

  "Be able to insert log and db record and detect inactive camera" in {

    whenReady(restClient.index(index, tpe, logRecord)) {
      _.created should be(true)
    }
    refresh()

    val end   = DateTime.now(DateTimeZone.UTC).getMillis
    val start   = DateTime.now(DateTimeZone.UTC).minusDays(1).getMillis

    cameraActivityRepo.getActiveCameras(SearchInterval(Some(start), Some(end)),1) map  { res =>
      logger.info(res.toString)
      res.isRight should be(true) }
  }

}
