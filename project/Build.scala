import sbt.Keys._
import sbt._
import sbtassembly.AssemblyPlugin.autoImport._

object Build extends Build {

  val domainVersion = "0.1.10-SNAPSHOT"
  val coreVersion = "0.1.11-SNAPSHOT"
  val traceVersion = "0.0.3-SNAPSHOT"
  val searchVersion = "0.0.2-SNAPSHOT"

  lazy val commonDependencies = Seq(
    "org.scalacheck"             %% "scalacheck"               % "1.12.5"%"test",
    "org.scalatest"               %% "scalatest"               % "2.2.4"   % "test" withSources() withJavadoc(),
    "org.mockito"                 %  "mockito-all"             % "1.10.19" % "test"
  )

  lazy val domainDependencies = Seq(
    "joda-time"                  %  "joda-time"                % "2.9.3",
    "org.joda"                   %  "joda-convert"             % "1.7",
    "io.argonaut"                %%  "argonaut"                % "6.1"
  )

  lazy val traceDependencies = Seq(
    "org.slf4j"                  % "slf4j-api"                  % "1.7.21",
    "com.cammy"                  %% "logging-domain"            % domainVersion,
    "ch.qos.logback"             % "logback-classic"            % "1.1.6" % "provided",
    "io.megam"                   %% "newman"                    % "1.3.12",
    "org.http4s"                 %% "http4s-dsl"                % "0.14.11",
    "org.scalaz"                 %% "scalaz-core"               % "7.1.1"
  )

  lazy val coreDependencies = Seq(
    "com.cammy"                  %% "logging-domain"            % domainVersion,
    //Utils
    "com.typesafe"               %  "config"                    % "1.3.0",
    "commons-lang"               %  "commons-lang"              % "2.6",
    //underlying loggers
    "org.slf4j"                  % "slf4j-api"                  % "1.7.21",
    "log4j"                      % "log4j"                      % "1.2.17",
    "ch.qos.logback"             % "logback-core"               % "1.1.6",
    "ch.qos.logback"             % "logback-classic"            % "1.1.6" % "provided",
    "com.typesafe.scala-logging" %% "scala-logging"             % "3.1.0"
  )

  lazy val searchDependencies = Seq(

    "com.typesafe"               %  "config"                    % "1.3.0",
    "org.scala-lang"             %  "scala-reflect"             % "2.11.8",
    "org.scala-lang.modules"     %% "scala-parser-combinators"  % "1.0.4",
    "org.scala-lang.modules"     %% "scala-xml"                 % "1.0.4",
    //Cammy
    "com.cammy"                  %% "logging-core"              % coreVersion,
    "com.cammy"                  %% "logging-domain"            % domainVersion,
    "com.cammy"                  %% "cammy-metrics-scala"       % "0.0.6",
    "com.cammy"                  %% "cammy-scala-util"          % "0.0.18",
    //Elastic
    "com.sumologic.elasticsearch" % "elasticsearch-core"        % "1.0.27" exclude("org.joda", "joda-convert"),
    "com.sumologic.elasticsearch" % "elasticsearch-aws"         % "1.0.27" withSources(),
    "com.sumologic.elasticsearch" % "elasticsearch-test"        % "1.0.27" withSources(),
    //JSON
    "io.argonaut"                %% "argonaut"                  % "6.1",
    "com.github.tototoshi"       %% "scala-csv"                 % "1.3.4",
    //Logging
    "com.typesafe.scala-logging" %% "scala-logging"             % "3.1.0",
    "org.slf4j"                  %  "slf4j-log4j12"             % "1.7.12",
    // Database
    "org.mongodb"                 %% "casbah"                   % "2.8.2",
    // HTTP
    "org.http4s"                  %% "http4s-blaze-server"      % "0.12.3" withSources(),
    "org.http4s"                  %% "http4s-dsl"               % "0.12.3" withSources(),
    "org.http4s"                  %% "http4s-argonaut"          % "0.12.3" withSources()
  )


  lazy val root = project.in(file(".")).settings (
  ).aggregate(domain,trace, core, search)

  lazy val domain = Project(
    id = "domain",
    base = file("domain"),
    settings = commonSettings ++ domainSettings ++ Seq(libraryDependencies ++= domainDependencies)
  )

  lazy val trace = Project(
    id = "trace",
    base = file("trace"),
    settings = commonSettings ++ traceSettings ++ Seq(libraryDependencies ++= traceDependencies) ++ Seq(libraryDependencies ++= commonDependencies)
  ).dependsOn (domain % "test->test;compile->compile")

  lazy val core = Project(
    id = "core",
    base = file("core"),
    settings = commonSettings ++ coreSettings ++ Seq(libraryDependencies ++= coreDependencies) ++ Seq(libraryDependencies ++= commonDependencies)
  ).dependsOn (domain % "test->test;compile->compile")


  lazy val search = Project(
    id = "search",
    base = file("search"),
    settings = commonSettings ++ searchSettings ++ Seq(libraryDependencies ++= searchDependencies) ++ Seq(libraryDependencies ++= commonDependencies)
  ).dependsOn (domain % "test->test;compile->compile", core % "test->test;compile->compile")


  lazy val domainSettings = Seq(
    name := "logging-domain",
    organization := "com.cammy",
    version := domainVersion
  )

  lazy val traceSettings = Seq(
    name := "logging-trace",
    organization := "com.cammy",
    version := traceVersion
  )

  lazy val coreSettings = Seq(
    name := "logging-core",
    organization := "com.cammy",
    version := coreVersion
  )

  lazy val searchSettings = Seq(
    name := "cammy-logger-api",
    organization := "com.cammy",
    version := searchVersion,
    mainClass in Compile := Some("com.cammy.logging.api.Boot"),
    testOptions in Test += Tests.Setup(classLoader =>
      classLoader
        .loadClass("org.slf4j.LoggerFactory")
        .getMethod("getLogger", classLoader.loadClass("java.lang.String"))
        .invoke(null, "ROOT")
    ),
    assemblyMergeStrategy in assembly := {
      case "log4j.properties"                            => MergeStrategy.first
      case "application.conf"                            => MergeStrategy.first
      case PathList("org", "joda", "convert", "FromString.class") => MergeStrategy.last
      case PathList("org", "joda", "convert", "ToString.class") => MergeStrategy.last
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    }
  )

  lazy val commonSettings = Seq(
    resolvers := commonResolvers,
    scalaVersion := "2.11.8",
    scalacOptions := Seq("-unchecked", "-feature", "-deprecation", "-encoding", "utf8", "-language:postfixOps", "-Xfatal-warnings"),
    parallelExecution in Test := false,
    publishTo := {
      val nexus = "http://nexus.cammy.com/nexus/"
      if (isSnapshot.value)
        Some("snapshots" at nexus + "content/repositories/snapshots")
      else
        Some("releases"  at nexus + "content/repositories/releases")
    },
    credentials += Credentials(
      "Nexus Repository Manager",
      "nexus.cammy.com",
      sys.env.getOrElse("NEXUS_USER", "admin"),
      sys.env.getOrElse("NEXUS_PASSWORD", "password")
    )
  )


  lazy val commonResolvers =  Seq(
    "tpolecat" at "http://dl.bintray.com/tpolecat/maven",
    "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases",
    "Newman" at "https://dl.bintray.com/megamsys/scala",
    "Cammy Snapshots" at "http://nexus.cammy.com/nexus/content/repositories/snapshots",
    "Cammy Release" at "http://nexus.cammy.com/nexus/content/repositories/releases",
    "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
    "Spring Maven Snapshots" at "http://repo.spring.io/libs-snapshot",
    "atlassian-public" at "https://maven.atlassian.com/content/groups/public/",
    "sonatype" at "https://oss.sonatype.org/content/repositories/snapshots",
    "OGI" at "http://maven.geo-solutions.it/",
    Resolver.sonatypeRepo("snapshots")
  )

}