package com.cammy.logging.domain.message

object LogLevel extends Enumeration {
  type level = Value
  val TRACE, DEBUG, INFO, WARN, ERROR, FATAL = Value
}
