package com.cammy.logging.domain.search

sealed trait SearchParam
case class SearchInterval(start: Option[Long] = None, end: Option[Long] = None) extends SearchParam
