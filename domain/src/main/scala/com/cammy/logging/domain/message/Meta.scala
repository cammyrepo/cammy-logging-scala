package com.cammy.logging.domain.message

case class Meta(meta: Map[String, String])


object MetaKey extends Enumeration {

  val THREAD_NAME = "thread_name"
  val FILE_NAME = "file"
  val LINE_NUMBER = "line_number"
  val CLASS_NAME = "class"
  val METHOD_NAME = "method"
  val LOG_VERSION = "log_version"
}