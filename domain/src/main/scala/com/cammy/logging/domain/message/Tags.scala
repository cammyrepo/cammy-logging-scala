package com.cammy.logging.domain.message

case class Tags(tags: Map[String, String])

object TagKey extends Enumeration {


  val CORRELATION_ID = "correlation_id"

  val ACCOUNT_ID = "account_id"
  val CAMERA_ID = "camera_id"
  val SNAPSHOT_ID = "snapshot_id"
  val EVENT_ID = "event_id"
  val HOME_ALARM_ID = "home_alarm_id"
  val NVR_ID = "nvr_id"
  val INCIDENT_ID = "incident_id"

}

object ContextTagKey {

  def apply(key: String): String = "CTX" + "-" + key

  def unapply(mdcKey: String) = {

    val keys = mdcKey split "-"
    if ((keys.length == 2) && keys(0) == "CTX")
      Some(keys(1))
    else None

  }

}







