package com.cammy.logging.domain.message

case class LogException(_class: Option[String],
                        message: Option[String],
                        stackTrace: Option[String])
