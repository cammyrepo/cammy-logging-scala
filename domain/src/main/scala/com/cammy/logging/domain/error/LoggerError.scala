package com.cammy.logging.domain.error

sealed trait LoggerError {
  def render() = msg
  def msg : String
}

case class LoggerEnqueueError(msg : String) extends LoggerError

trait LoggerClientError extends LoggerError
case class LoggerClientInitializationError(msg : String) extends LoggerClientError
case class LoggerClientValidationError(msg : String) extends LoggerClientError
case class LoggerClientWriteError(msg : String) extends LoggerClientError
case class LoggerClientShutdownError(msg : String) extends LoggerClientError

trait LoggerServiceError extends LoggerError
case class LoggerFatalServiceError(msg : String) extends LoggerServiceError
case class LoggerRecoverableServiceError(msg : String) extends LoggerServiceError
case class LoggerThrottlingServiceError(msg : String) extends LoggerServiceError




