package com.cammy.logging.domain.message

import java.util.UUID

import org.joda.time.DateTime



  case class LogMessage[A](
                            appName: String, // firehose, info-type: static
                            appVersion: String, // 0.0.9,  info-type:static
                            appEnv: String, // staging, info-type:static
                            instanceId: Option[String], // i-05011fda, info-type: static
                            timeStamp: String, // 2016-06-14T00:10:01.636Z, info-type: dynamic
                            level: Option[LogLevel.level], //  INFO, info-type:dynamic
                            correlationId: Option[UUID], // f53fb7cb-c005-420b-8503-f991bf3e2b34 info_type: session
                            message: A, //  "Insufficient funds for user: xxx", info-type: dynamic
                            exception: Option[LogException], //  "Braintree Error", info-type: dynamic
                            tags: Tags, //  [(cameraId, xxx)], info-type: session
                            meta: Meta //  [(lineNumber, xxx)], info-type: session and dynamic
                       )


















