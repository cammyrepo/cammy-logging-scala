package com.cammy.logging.domain.error

sealed trait SearchError {
  val reason: String
  def toThrowable = new Exception(reason)
}

case class ESConnectionError(reason: String) extends SearchError
case class ESLogSearchError(reason: String) extends SearchError
case class ESMongoSearchError(reason: String) extends SearchError
case class InvalidSearchLimitError(reason: String) extends SearchError
case class InvalidSearchParamError(reason: String) extends SearchError
