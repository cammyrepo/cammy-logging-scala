package com.cammy.logging.domain.camera

import java.text.SimpleDateFormat

import argonaut.Argonaut._
import argonaut._

import scala.language.implicitConversions
import scala.util.Try

case class Camera(id: String,
                  owner: String,
                  creationDate: BigInt,
                  lastEventTime: Double,
                  lastLogTime: Option[Long])

object Camera {

  val  format = new SimpleDateFormat("dd/MM/yyyy")

  implicit val encodeJson: EncodeJson[Camera] = EncodeJson(
      (c: Camera) =>
          ("camera_id" := c.id) ->:
          ("owner" := c.owner) ->:
          ("creation_date" := c.creationDate) ->:
          ("last_eventTime" := c.lastEventTime) ->:
          c.lastLogTime.map("last_logTime" := _) ->?: jEmptyObject)

  implicit val decodeJson: DecodeJson[Camera] = DecodeJson(c =>
        for {
      id <- (c --\ "camera_id").as[String]
      owner <- (c --\ "owner").as[String]
      creationDate <- (c --\ "creation_date").as[BigInt]
      lastEventTime <- (c --\ "last_eventTime").as[Double]
      lastLogTime <- (c --\ "last_logTime").as[Option[Long]]
    } yield Camera(id, owner, creationDate, lastEventTime, lastLogTime))

  implicit val codecJson: CodecJson[Camera] = CodecJson(
      encodeJson.encode,
      decodeJson.decode
  )

  implicit def fromMap(map: Map[String, Any]): Camera = {
    Camera(
        map.getOrElse("camera_id", "unknown").asInstanceOf[String].toUpperCase,
        map.getOrElse("owner", "").asInstanceOf[String],
        Try(map.getOrElse("date_created",0).asInstanceOf[BigInt]).getOrElse(0),
        map.get("last_event") match {
          case Some(event) =>
            val eventDetail = event.asInstanceOf[Map[String, Double]]
            Try(eventDetail.getOrElse("start_timestamp", 0.0)).getOrElse(0.0)
          case None => 0.0
        },
        Try(map.getOrElse("last_logTime",0).asInstanceOf[Option[Long]]).getOrElse(Some(0))
    )
  }

  implicit  def toCSVSeq(camera : Camera) : Seq[String] = {
    Seq(camera.id,
      camera.owner.toLowerCase,
      if(camera.creationDate.isValidLong && camera.creationDate != BigInt(0))
        format.format(camera.creationDate * 1000)
      else  "",
      if(camera.lastEventTime != 0.0)
        format.format(camera.lastEventTime.toLong * 1000 )
      else ""
     // camera.lastLogTime.fold("")(date=> Try(format.format(date)).getOrElse(""))
    )
  }
}
