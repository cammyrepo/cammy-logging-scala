package com.cammy.logging.domain.camera

sealed trait CameraStatus

object Active extends CameraStatus
object Inactive extends CameraStatus
