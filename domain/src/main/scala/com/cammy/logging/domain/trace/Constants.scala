package com.cammy.logging.domain.trace


object Constants {
  val CORRELATION_ID_HEADER = "X-Correlation-Id"
}
