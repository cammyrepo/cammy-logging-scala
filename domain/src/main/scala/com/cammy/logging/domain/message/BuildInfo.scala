package com.cammy.logging.domain.message

case class BuildInfo(appName: String, appVersion: String, appEnv: String)