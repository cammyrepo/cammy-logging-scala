package com.cammy.logging.domain.message

object ESFieldName extends Enumeration {

  val APP_NAME = "app_name"
  val APP_VERSION = "app_version"
  val APP_ENV = "env"
  val INSTANCE_ID = "instance_id"
  val TIME_STAMP = "@timestamp"
  val CORRELATION_KEY = "correlation_id"
  val MESSAGE = "message"
  val LOG_LEVEL = "log_level"
  val EXCEPTION = "exception"
  val EXCEPTION_CLASS = "class"
  val EXCEPTION_MESSAGE = "message"
  val EXCEPTION_STACKTRACE = "stacktrace"
  val TAGS = "tags"
  val META = "meta"

}
